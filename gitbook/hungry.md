# 訂便當

###### tags: `後端文件`

## 伺服器網址

[正式機](https://center.tgfc.tw:4443/)
[測試機](http://192.168.1.236:8090/)
[開發機](http://172.0.10.212:8090/)

## Gitlab

~~https://git.tgfc.tw/java-team/hungry~~

## Showdoc

~~https://showdoc.opscloud.info/web/#/3
http://192.168.1.10:4999/web/#/3~~

## v2.1 用文件

[新增/修改 API 檢索表](http://172.0.10.211:3000/ux8RFaeZSRubF-ZRpp7QEw)
[新增/修改 API 一覽表](https://eip.tgfc.tw/office/oo/r/542205892523734527#tid=1)

## 其他文件

[使用手冊(水餃)](http://172.0.10.211:3000/s/HyJGeSAnS)
